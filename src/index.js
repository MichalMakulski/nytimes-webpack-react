import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app'
import appState from './redux/store';

appState.subscribe(render);

render();

function render () {
    ReactDOM.render(
    <App 
        store = { appState } 
        state = { appState.getState() } 
    />, document.getElementById('app'));
}
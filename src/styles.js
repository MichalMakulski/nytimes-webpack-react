const global = {
    fontFamily: 'sans-serif',
    color: '#383838'
}

const readList = {
    readListBtn: {
        fontWeight: 700,
        textAlign: 'right',
        cursor: 'pointer'
    },
    list: {
        transition: 'all .3s ease-out',
        overflow: 'auto'
    }
}

const form = {
    label: {
        display: 'inline-block',
        marginTop: 10
    },
    submitBtn: {
        marginTop: 15,
        backgroundColor: 'transparent',
        border: '1px solid',
        cursor: 'pointer',
        padding: '5px 15px'
    }
}

const articleList = {
    loader: {
        textAlign: 'center'
    }
}

const articleItem = {
    item: {
      listStyle: 'none'  
    },
    img: {
        verticalAlign: 'middle',
        marginRight: 10
    },
    author: {
        textAlign: 'right'
    },
    addBtn: {
        backgroundColor: 'transparent',
        color: '#33cc33',
        marginLeft: 10,
        cursor: 'pointer',
        border: '2px solid #33cc33',
        padding: '5px 15px'
    }
}

export { global, readList, form, articleList, articleItem };
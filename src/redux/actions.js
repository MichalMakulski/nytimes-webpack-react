import get from '../ajax';

export { getArticles, updateInput, saveArticle, toggleReadList };

function getArticles (dispatch, state) {
    dispatch(loadingResults(true, null));
    
    const ROOT_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';
    const apiKey = '?api-key=370921d142394b939dd222b4726cabd4';
    const keyword = state.query.keyword;
    const beginDate = state.query.beginDate || getLastMonthRange().beginDate;
    const endDate = state.query.endDate || getLastMonthRange().endDate;
    const queryStr = `&q=${keyword}&begin_date=${beginDate}&end_date=${endDate}`;
    
    get(ROOT_URL + apiKey, queryStr).then(dispatchResultsReady);
    
    function dispatchResultsReady (articlesData) {
        dispatch(loadingResults(false, articlesData))      
    }
}

function getLastMonthRange () {
    const today = new Date();
    const day = today.getDate().toString();
    const month = (today.getMonth() + 1).toString();
    const year = today.getFullYear().toString();

    return {
        beginDate: year + (month.length === 1 ? '0' + month : month) + '01',
        endDate: year + (month.length === 1 ? '0' + month : month) + (day.length === 1 ? '0' + day : day)
    }
}

function loadingResults (status, articlesData) {
    return {
        type: 'AJAX_REQUEST',
        loadingResults: status,
        articles: articlesData
    }
}

function updateInput (ev) {
    const target = ev.target;
    const input = target.id;
    const value = target.value.replace(/-/g, '');
    
    return {
        type: 'USER_INPUT',
        input: input,
        value: value
    }
}

function saveArticle (savedArticle) {
    const id = savedArticle.id;
    const title = savedArticle.title;
    const href = savedArticle.href;
    
    return {
        type: 'SAVE_ARTICLE',
        id,
        title,
        href
    }
}

function toggleReadList () {
    return {
        type: 'TOGGLE_READ_LIST'
    }
}
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { thunk } from './middleware';
import handleMyArticles from '../myArticles-handler';

const INITIAL_STATE = {
    query: {
        keyword: '',
        beginDate: '',
        endDate: ''
    },
    results: {
        loadingResults: false,
        articles: null
    },
    myArticles: handleMyArticles('get'),
    readListVisible: false
}
const appReducer = combineReducers({
    results,
    query,
    myArticles,
    readListVisible
});

export default createStore(appReducer, applyMiddleware(thunk));

function results (state = INITIAL_STATE.results, action) {
    const {type, articles, loadingResults} = action;
    
    if (type === 'AJAX_REQUEST') {
        return {...state, 
            articles, 
            loadingResults
        };
    }
    
    return state;
}

function query (state = INITIAL_STATE.query, action) {
    const {type, input, value} = action;
    
    if (type === 'USER_INPUT') {
        return {...state, [input]: value}
    } 
    
    return state;
}

function myArticles (state = INITIAL_STATE.myArticles, action) {
    const {type, title, href, id} = action;
    
    if (type === 'SAVE_ARTICLE') {
        return [...state, {title, href, id}]
    } 
    
    return state;
}

function readListVisible (state = INITIAL_STATE.readListVisible, action) {
    const { type } = action;
    
    if (type === 'TOGGLE_READ_LIST') {
        return !state;
    } 
    
    return state;
}
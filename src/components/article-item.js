import React from 'react';
import { articleItem } from '../styles';

export default React.createClass({
    render () {   
        const article = this.props.articleData;
        const { headline:title, web_url: href, snippet: snippet, byline: author } = article;
        const thumbnail = article.multimedia.filter(image => image.width === 75);
        const thumbnailUrl = thumbnail[0] ? `http://www.nytimes.com/${thumbnail[0].url}` : 'http://placehold.it/75x75';
        
        return <li style={articleItem.item} className="articles-item">
            <h2>
                <img style={articleItem.img} src={thumbnailUrl} />
                <a href={href}>{title && title.main}</a>
            </h2>
            <p>{snippet}</p>
            <p style={articleItem.author}>
                {author && author.original}
                <button style={articleItem.addBtn} href="" onClick={this.saveArticle}>Add to read list</button>
            </p>
        </li>
    },
    saveArticle (ev) {
        const article = this.props.articleData;
        const { _id: id, headline:title, web_url: href } = article;
        
        this.props.saveArticle({id, title, href});
    }
});
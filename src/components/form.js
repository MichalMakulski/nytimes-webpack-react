import React from 'react';
import { getArticles, updateInput } from '../redux/actions';
import { form } from '../styles';

export default React.createClass({
    render () {
        return <div>
            <form method="GET" action="" onSubmit={this.submitHandler}>
                <label style={form.label} htmlFor="keyword">Keyword</label><br />
                <input type="text" id="keyword" onInput={this.updateStateWithUserInput}/><br />
                <label style={form.label} htmlFor="beginDate">Start date</label><br />
                <input type="date" id="beginDate" onInput={this.updateStateWithUserInput}/><br />
                <label style={form.label} htmlFor="endDate">End date</label><br />
                <input type="date" id="endDate" onInput={this.updateStateWithUserInput}/><br />
                <input style={form.submitBtn} type="submit" value="Search" />    
            </form>
        </div>
    },
    submitHandler (ev) {
        ev.preventDefault();
        const {store, state} = this.props;
        const keyword = state.query.keyword;
        
        if (!keyword) {
            return;
        }
        
        store.dispatch(getArticles);
    },
    updateStateWithUserInput (ev) {
        const target = ev.target;
        const {store} = this.props;
        const input = target.id;
        const value = target.value.replace(/-/g, '');
        
        store.dispatch(updateInput(ev));
    }
});
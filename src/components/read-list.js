import React from 'react';
import ArticleItem from './article-item';
import appState from '../redux/store';
import { readList } from '../styles';

export default React.createClass({
    render () {
        const myArticles = this.props.myArticles.map((article) => {
            return <li key={article.id}>
                <a href={article.href}>{article.title.main}</a>
            </li>
        });
        const myArticlesNumber = myArticles.length;
        const listHeight = myArticlesNumber > 10 ? 200 : myArticlesNumber * 18;
        const readListVisible = this.props.readListVisible ?  listHeight + 'px' : '0';
        
        if (!myArticles.length) {
            return <h4>Click "Add to read list" to add item here.</h4>
        }
        
        return <div>
            <ul style={{...readList.list, height: readListVisible}}>
                {myArticles}
            </ul>
            <p 
                style={readList.readListBtn}
                onClick={this.props.toggleReadList}
            >
                My Articles
            </p>
        </div>
    }
    
});
import React from 'react';
import ArticleItem from './article-item';
import { articleList } from '../styles';

export default React.createClass({
    render () {
        const articleItems = this.parseArticlesData(this.props.articles);
        const { state } = this.props;
        const keyword = state.query.keyword;
        const isLoadingResults = state.results.loadingResults;
        
        if (isLoadingResults) {
            return <p style={articleList.loader}>Loading...</p>;
        }
        
        if (!articleItems) {
            return <p>Enter the keyword. If no dates are provided, search will be done for this month.</p>
        }
        
        if (articleItems.length === 0) {
            return <h2>{`No results for "${keyword}" found.`}</h2>;
        }
        
        return <div>
            <h2>{`Results for "${keyword}":`}</h2>
            <ul className="articles-list">
                { articleItems }
            </ul>
        </div>
    },
    parseArticlesData (data) {

        if (!data) {
            return null;
        }
        
        const articlesData = JSON.parse(data);
        const articles = articlesData && articlesData.response.docs;
        
        return articles.map((article, idx) => {
            return <ArticleItem 
                key={article._id} 
                articleData={article}
                saveArticle={this.props.saveArticle}
            />    
        });
    }
});
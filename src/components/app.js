import React from 'react';
import { global } from '../styles';
import Form from './form';
import ArticlesList from './articles-list';
import ReadList from './read-list';
import { saveArticle, toggleReadList } from '../redux/actions';
import handleMyArticles from '../myArticles-handler';

export default React.createClass({
    render () {
        const { state, store } = this.props;
        const myArticles = state.myArticles;
        const articles = state.results.articles;
        
        return <div style={global}>
            <ReadList 
                myArticles={myArticles}
                toggleReadList={this.toggleReadList}
                readListVisible={state.readListVisible}
            />
            <Form 
                state={state}
                store={store}
            />
            <ArticlesList 
                articles={articles}
                saveArticle={this.saveArticle}
                state={ state }
            />   
        </div>
    },
    saveArticle (savedArticle) {
        const { store, state } = this.props;
        const myArticles = state.myArticles;
        const isArticleAlreadySaved = myArticles.filter(article => article.id === savedArticle.id)
        
        if (isArticleAlreadySaved.length) {
            alert(`Article "${savedArticle.title.main}" is already on Read List.`);
            return;
        }
        
        store.dispatch(saveArticle(savedArticle));
        handleMyArticles('save', savedArticle);
    },
    toggleReadList () {
        const { store } = this.props;
        store.dispatch(toggleReadList());
    }
});
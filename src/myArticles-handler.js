export default function handleMyArticles (action, article) {
    const ls = window.localStorage;
    
    if (action === 'get') {
        return ls.myArticles ? JSON.parse(ls.myArticles) : []
    }
    
    if (action === 'save') {
        if (ls.myArticles) {
            let myArticles = JSON.parse(ls.myArticles);
            myArticles.push(article);
            ls.setItem('myArticles', JSON.stringify(myArticles))
        } else {
            ls.setItem('myArticles', JSON.stringify([article]))
        }
    }
}
export default function get (src, query = '') {
    const xmlhttp = new XMLHttpRequest(); 
    
    return new Promise((resolve, reject) => {
        xmlhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                resolve(this.responseText);
            }
        };
        xmlhttp.open("GET", src + query, true);
        xmlhttp.send();    
    });
}